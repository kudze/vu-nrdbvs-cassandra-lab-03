<?php

namespace Kudze\CassandraDemo\Repository;

use Kudze\CassandraDemo\Service\Cassandra;

class Repository
{
    protected Cassandra $cassandra;

    public function __construct(Cassandra $cassandra)
    {
        $this->cassandra = $cassandra;
    }

    /**
     * @return Cassandra
     */
    public function getCassandra(): Cassandra
    {
        return $this->cassandra;
    }
}