<?php

namespace Kudze\CassandraDemo\Repository;

use JetBrains\PhpStorm\Pure;
use Kudze\CassandraDemo\Exceptions\CompanyInsertIdAlreadyInUse;
use Kudze\CassandraDemo\Exceptions\UserInsertEmailAlreadyInUseException;
use Kudze\CassandraDemo\Models\User;
use Kudze\CassandraDemo\Service\Cassandra;

class UserRepository extends Repository
{
    public function insertUser(User $user): void
    {
        $session = $this->cassandra->getSession();
        $table = $this->getFullTableName();

        $result = $session->execute(
            "INSERT INTO $table (email, password) " .
            "VALUES (?, ?) IF NOT EXISTS",
            [
                'arguments' => $user->toArray()
            ]
        )->first();
        if (!$result['[applied]'])
            throw new UserInsertEmailAlreadyInUseException();
    }

    public function getAllUsers(): array
    {
        $session = $this->cassandra->getSession();
        $table = $this->getFullTableName();

        $result = [];
        $rows = $session->execute("SELECT * FROM $table", []);
        foreach($rows as $row)
            $result[] = new User($row['email'], $row['password']);

        return $result;
    }

    #[Pure]
    public static function getFullTableName(): string
    {
        return Cassandra::constructFullTablePath('users');
    }
}