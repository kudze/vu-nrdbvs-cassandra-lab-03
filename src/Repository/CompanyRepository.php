<?php

namespace Kudze\CassandraDemo\Repository;

use JetBrains\PhpStorm\Pure;
use Kudze\CassandraDemo\Exceptions\CompanyInsertIdAlreadyInUse;
use Kudze\CassandraDemo\Models\Company;
use Kudze\CassandraDemo\Service\Cassandra;

class CompanyRepository extends Repository
{
    public function insertCompany(Company $company): void
    {
        $session = $this->cassandra->getSession();
        $table = $this->getFullTableName();

        $result = $session->execute(
            "INSERT INTO $table (id, title) " .
            "VALUES (?, ?) IF NOT EXISTS",
            [
                'arguments' => $company->toArray()
            ]
        )->first();
        if (!$result['[applied]'])
            throw new CompanyInsertIdAlreadyInUse();
    }

    public function getAllCompanies(): array
    {
        $session = $this->cassandra->getSession();
        $table = $this->getFullTableName();

        $result = [];
        $rows = $session->execute("SELECT * FROM $table", []);
        foreach ($rows as $row)
            $result[] = new Company($row['id'], $row['title']);

        return $result;
    }

    #[Pure]
    public static function getFullTableName(): string
    {
        return Cassandra::constructFullTablePath('companies');
    }
}