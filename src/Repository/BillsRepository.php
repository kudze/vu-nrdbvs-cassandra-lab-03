<?php

namespace Kudze\CassandraDemo\Repository;

use Cassandra\Uuid;
use JetBrains\PhpStorm\Pure;
use Kudze\CassandraDemo\Exceptions\BillInsertIdAlreadyInUse;
use Kudze\CassandraDemo\Models\Bill;
use Kudze\CassandraDemo\Service\Cassandra;

class BillsRepository extends Repository
{
    public function getAllBills(): array
    {
        $session = $this->cassandra->getSession();
        $table = self::getIDTableName();

        $result = [];
        $rows = $session->execute("SELECT * FROM $table", []);
        foreach ($rows as $row)
            $result[] = new Bill($row['id'], $row['company_id'], $row['user_email'], $row['amount']);

        return $result;
    }

    public function getBillsByUserEmail(string $userEmail): array
    {
        $session = $this->cassandra->getSession();
        $table = self::getUserEmailTableName();

        $result = [];
        $rows = $session->execute("SELECT * FROM $table WHERE user_email = ?", [
            'arguments' => [$userEmail]
        ]);
        foreach ($rows as $row)
            $result[] = new Bill($row['id'], $row['company_id'], $row['user_email'], $row['amount']);

        return $result;
    }

    public function getBillsByCompanyId(Uuid $companyId): array
    {
        $session = $this->cassandra->getSession();
        $table = self::getCompanyIdTableName();

        $result = [];
        $rows = $session->execute("SELECT * FROM $table WHERE company_id = ?", [
            'arguments' => [$companyId]
        ]);
        foreach ($rows as $row)
            $result[] = new Bill($row['id'], $row['company_id'], $row['user_email'], $row['amount']);

        return $result;
    }

    public function insertBill(Bill $bill): void
    {
        $session = $this->cassandra->getSession();
        $table = self::getIDTableName();

        $result = $session->execute(
            "INSERT INTO $table (id, company_id, user_email, amount) " .
            "VALUES (?, ?, ?, ?) IF NOT EXISTS",
            [
                'arguments' => $bill->toArray()
            ]
        )->first();
        if (!$result['[applied]'])
            throw new BillInsertIdAlreadyInUse();

        $table = self::getCompanyIdTableName();
        $session->execute(
            "INSERT INTO $table (id, company_id, user_email, amount) " .
            "VALUES (?, ?, ?, ?)",
            [
                'arguments' => $bill->toArray()
            ]
        );

        $table = self::getUserEmailTableName();
        $session->execute(
            "INSERT INTO $table (id, company_id, user_email, amount) " .
            "VALUES (?, ?, ?, ?)",
            [
                'arguments' => $bill->toArray()
            ]
        );
    }

    #[Pure]
    public static function getIDTableName(): string
    {
        return Cassandra::constructFullTablePath('bills_id');
    }

    #[Pure]
    public static function getUserEmailTableName(): string
    {
        return Cassandra::constructFullTablePath('bills_users');
    }

    #[Pure]
    public static function getCompanyIdTableName(): string
    {
        return Cassandra::constructFullTablePath('bills_companies');
    }
}