<?php

namespace Kudze\CassandraDemo\Service;

use Kudze\CassandraDemo\Models\Bill;
use Kudze\CassandraDemo\Models\Company;
use Kudze\CassandraDemo\Models\User;
use LucidFrame\Console\ConsoleTable;

class TableFormatter
{
    public function formatUsers(array $users): string
    {
        $table = new ConsoleTable();
        $table
            ->addHeader('Email')
            ->addHeader('Password hash');
        foreach ($users as $user) {
            /** @var User $user */
            $table->addRow()
                ->addColumn($user->getEmail())
                ->addColumn($user->getPasswordHash());
        }

        $table->setPadding(2);
        return $table->getTable();
    }

    public function formatCompanies(array $companies): string
    {
        $table = new ConsoleTable();
        $table
            ->addHeader('ID')
            ->addHeader('Password hash');
        foreach ($companies as $company) {
            /** @var Company $company */
            $table->addRow()
                ->addColumn($company->getId()->uuid())
                ->addColumn($company->getTitle());
        }

        $table->setPadding(2);
        return $table->getTable();
    }

    public function formatBills(array $bills): string
    {
        $table = new ConsoleTable();
        $table
            ->addHeader('ID')
            ->addHeader('Company ID')
            ->addHeader('User\'s email')
            ->addHeader('Amount');
        foreach ($bills as $bill) {
            /** @var Bill $bill */
            $table->addRow()
                ->addColumn($bill->getId()->uuid())
                ->addColumn($bill->getCompanyId()->uuid())
                ->addColumn($bill->getUserEmail())
                ->addColumn($bill->getAmount()->toDouble());
        }

        $table->setPadding(2);
        return $table->getTable();
    }
}