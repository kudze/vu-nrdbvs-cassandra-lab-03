<?php

namespace Kudze\CassandraDemo\Models;

use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class User
{
    protected string $email;
    protected string $passwordHash;

    public function __construct(string $email, string $passwordHash)
    {
        $this->email = $email;
        $this->passwordHash = $passwordHash;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }

    #[Pure]
    #[ArrayShape(['email' => "string", 'password' => "string"])]
    public function toArray(): array
    {
        return [
            'email' => $this->getEmail(),
            'password' => $this->getPasswordHash()
        ];
    }
}