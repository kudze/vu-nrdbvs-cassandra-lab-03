<?php

namespace Kudze\CassandraDemo\Models;

use Cassandra\Uuid;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class Company
{
    private Uuid $id;
    private string $title;

    public function __construct(Uuid $id, string $title)
    {
        $this->id = $id;
        $this->title = $title;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    #[Pure]
    #[ArrayShape(['id' => "\Cassandra\Uuid", 'title' => "string"])]
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle()
        ];
    }
}