<?php

namespace Kudze\CassandraDemo\Models;

use Cassandra\Decimal;
use Cassandra\Uuid;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class Bill
{
    private Uuid $id;
    private Uuid $companyId;
    private string $userEmail;
    private Decimal $amount;

    public function __construct(Uuid $id, Uuid $companyId, string $userEmail, Decimal $amount)
    {
        $this->id = $id;
        $this->companyId = $companyId;
        $this->userEmail = $userEmail;
        $this->amount = $amount;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return Uuid
     */
    public function getCompanyId(): Uuid
    {
        return $this->companyId;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->userEmail;
    }

    /**
     * @return Decimal
     */
    public function getAmount(): Decimal
    {
        return $this->amount;
    }

    #[Pure]
    #[ArrayShape(['id' => "\Cassandra\Uuid", 'company_id' => "\Cassandra\Uuid", 'user_email' => "string", 'amount' => "\Cassandra\Decimal"])]
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'company_id' => $this->getCompanyId(),
            'user_email' => $this->getUserEmail(),
            'amount' => $this->getAmount()
        ];
    }
}