<?php

namespace Kudze\CassandraDemo\Command;

use Kudze\CassandraDemo\Exceptions\UserInsertEmailAlreadyInUseException;
use Kudze\CassandraDemo\Models\User;
use Kudze\CassandraDemo\Repository\UserRepository;
use Kudze\CassandraDemo\Service\Hasher;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RegisterUserCommand extends Command
{
    protected const ARG_EMAIL = "email";
    protected const ARG_PASSWORD = "password";

    protected Hasher $hasher;
    protected UserRepository $userRepository;

    public function __construct(Hasher $hasher, UserRepository $userRepository)
    {
        parent::__construct();

        $this->hasher = $hasher;
        $this->userRepository = $userRepository;
    }

    protected function configure()
    {
        $this->setName("register");
        $this->setDescription("Registers an user");

        $this->addArgument(self::ARG_EMAIL, InputArgument::REQUIRED, "Email to register account with");
        $this->addArgument(self::ARG_PASSWORD, InputArgument::REQUIRED, "Password to register account with");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $email = $input->getArgument(self::ARG_EMAIL);
        $password = $input->getArgument(self::ARG_PASSWORD);
        $password_hashed = $this->hasher->hash($password);

        $user = new User($email, $password_hashed);

        try {
            $this->userRepository->insertUser($user);
            $output->writeln("User with email \"$email\" has been successfully created!");
        } catch(UserInsertEmailAlreadyInUseException) {
            $output->writeln("User with email \"$email\" is already registered!");
        }

        return Command::SUCCESS;
    }

}