<?php

namespace Kudze\CassandraDemo\Command;

use Cassandra\Decimal;
use Cassandra\Uuid;
use Kudze\CassandraDemo\Exceptions\BillInsertIdAlreadyInUse;
use Kudze\CassandraDemo\Models\Bill;
use Kudze\CassandraDemo\Repository\BillsRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateBillCommand extends Command
{
    protected const ARG_COMPANY_ID = "company_id";
    protected const ARG_USER_EMAIL = "user_email";
    protected const ARG_AMOUNT = "amount";

    protected BillsRepository $billsRepository;

    public function __construct(BillsRepository $billsRepository)
    {
        parent::__construct();

        $this->billsRepository = $billsRepository;
    }

    protected function configure()
    {
        $this->setName("create-bill");
        $this->setDescription("Creates a bill");

        $this->addArgument(self::ARG_COMPANY_ID, InputArgument::REQUIRED, "ID of an company");
        $this->addArgument(self::ARG_USER_EMAIL, InputArgument::REQUIRED, "User's email");
        $this->addArgument(self::ARG_AMOUNT, InputArgument::REQUIRED, "Bill amount");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $companyId = $input->getArgument(self::ARG_COMPANY_ID);
        $userEmail = $input->getArgument(self::ARG_USER_EMAIL);
        $amount = $input->getArgument(self::ARG_AMOUNT);

        $companyUuid = new Uuid($companyId);
        $amountDecimal = new Decimal(number_format($amount, 2, '.', ''));

        while(true) {
            try {
                $bill = new Bill(new Uuid(), $companyUuid, $userEmail, $amountDecimal);
                $this->billsRepository->insertBill($bill);

                $output->writeln("Bill has been successfully added!");
                break;
            } catch (BillInsertIdAlreadyInUse) {
                $output->writeln("Randomly picked UUID for bill is already in use, retrying with another one...");
                continue;
            }
        }

        return Command::SUCCESS;
    }

}