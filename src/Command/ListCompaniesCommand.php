<?php

namespace Kudze\CassandraDemo\Command;

use Kudze\CassandraDemo\Repository\CompanyRepository;
use Kudze\CassandraDemo\Service\TableFormatter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListCompaniesCommand extends Command
{
    protected CompanyRepository $companyRepository;
    protected TableFormatter $tableFormatter;

    public function __construct(CompanyRepository $companyRepository, TableFormatter $tableFormatter)
    {
        parent::__construct();

        $this->companyRepository = $companyRepository;
        $this->tableFormatter = $tableFormatter;
    }

    protected function configure()
    {
        $this->setName("companies");
        $this->setDescription("Lists all companies");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $companies = $this->companyRepository->getAllCompanies();
        $table = $this->tableFormatter->formatCompanies($companies);
        $output->writeln($table);

        return Command::SUCCESS;
    }

}