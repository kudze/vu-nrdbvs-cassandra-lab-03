<?php

namespace Kudze\CassandraDemo\Command;

use Kudze\CassandraDemo\Repository\BillsRepository;
use Kudze\CassandraDemo\Service\TableFormatter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListUserBillsCommand extends Command
{
    const ARG_EMAIL = "user_email";

    protected BillsRepository $billsRepository;
    protected TableFormatter $tableFormatter;

    public function __construct(BillsRepository $billsRepository, TableFormatter $tableFormatter)
    {
        parent::__construct();

        $this->billsRepository = $billsRepository;
        $this->tableFormatter = $tableFormatter;
    }

    protected function configure()
    {
        $this->setName("user-bills");
        $this->setDescription("Lists all user's bills");

        $this->addArgument(self::ARG_EMAIL, InputArgument::REQUIRED, "Email of user");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $email = $input->getArgument(self::ARG_EMAIL);

        $bills = $this->billsRepository->getBillsByUserEmail($email);
        $table = $this->tableFormatter->formatBills($bills);
        $output->writeln($table);

        return Command::SUCCESS;
    }

}