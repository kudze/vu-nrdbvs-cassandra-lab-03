<?php

namespace Kudze\CassandraDemo\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HelloWorldCommand extends Command
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName("hello-world");
        $this->setDescription("Greets user with hello world!");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("Hello World!");
        return Command::SUCCESS;
    }
}