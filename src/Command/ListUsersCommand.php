<?php

namespace Kudze\CassandraDemo\Command;

use Kudze\CassandraDemo\Repository\UserRepository;
use Kudze\CassandraDemo\Service\TableFormatter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListUsersCommand extends Command
{
    protected UserRepository $userRepository;
    protected TableFormatter $tableFormatter;

    public function __construct(UserRepository $userRepository, TableFormatter $tableFormatter)
    {
        parent::__construct();

        $this->userRepository = $userRepository;
        $this->tableFormatter = $tableFormatter;
    }

    protected function configure()
    {
        $this->setName("users");
        $this->setDescription("Lists all users");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $users = $this->userRepository->getAllUsers();
        $table = $this->tableFormatter->formatUsers($users);
        $output->writeln($table);

        return Command::SUCCESS;
    }

}