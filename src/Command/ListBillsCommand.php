<?php

namespace Kudze\CassandraDemo\Command;

use Kudze\CassandraDemo\Repository\BillsRepository;
use Kudze\CassandraDemo\Service\TableFormatter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListBillsCommand extends Command
{
    protected BillsRepository $billsRepository;
    protected TableFormatter $tableFormatter;

    public function __construct(BillsRepository $billsRepository, TableFormatter $tableFormatter)
    {
        parent::__construct();

        $this->billsRepository = $billsRepository;
        $this->tableFormatter = $tableFormatter;
    }

    protected function configure()
    {
        $this->setName("bills");
        $this->setDescription("Lists all bills");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $bills = $this->billsRepository->getAllBills();
        $table = $this->tableFormatter->formatBills($bills);
        $output->writeln($table);

        return Command::SUCCESS;
    }

}