<?php

namespace Kudze\CassandraDemo\Command;

use Cassandra\SimpleStatement;
use Kudze\CassandraDemo\Repository\BillsRepository;
use Kudze\CassandraDemo\Repository\CompanyRepository;
use Kudze\CassandraDemo\Repository\UserRepository;
use Kudze\CassandraDemo\Service\Cassandra;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetupCassandraCommand extends Command
{
    protected Cassandra $cassandra;

    public function __construct(Cassandra $cassandra)
    {
        parent::__construct();

        $this->cassandra = $cassandra;
    }

    protected function configure()
    {
        $this->setName("init");
        $this->setDescription("initializes cassandra tables");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $session = $this->cassandra->getSession();

        //USER TABLE.
        $output->writeln("Creating users table...");
        $table = UserRepository::getFullTableName();
        $this->cassandra->dropTableIfExists($table);
        $cql =
            <<<EOD
            CREATE TABLE $table(
                    email text PRIMARY KEY,
                    password text,
            )
            EOD;
        $session->execute(new SimpleStatement($cql), []);

        //COMPANY TABLE.
        $output->writeln("Creating companies table...");
        $table = CompanyRepository::getFullTableName();
        $this->cassandra->dropTableIfExists($table);
        $cql =
            <<<EOD
            CREATE TABLE $table(
                    id uuid PRIMARY KEY,
                    title text,
            )
            EOD;
        $session->execute(new SimpleStatement($cql), []);

        //BILLS TABLE (PK = ID)
        $output->writeln("Creating bills by id table...");
        $table = BillsRepository::getIDTableName();
        $this->cassandra->dropTableIfExists($table);
        $cql =
            <<<EOD
            CREATE TABLE $table(
                    id uuid PRIMARY KEY,
                    company_id uuid,
                    user_email text,
                    amount decimal
            )
            EOD;
        $session->execute(new SimpleStatement($cql), []);

        //BILLS TABLE (PK = Company Id)
        $output->writeln("Creating bills by company id...");
        $table = BillsRepository::getCompanyIdTableName();
        $this->cassandra->dropTableIfExists($table);
        $cql =
            <<<EOD
            CREATE TABLE $table(
                    id uuid,
                    company_id uuid,
                    user_email text,
                    amount decimal,
                    PRIMARY KEY (company_id, id)
            )
            EOD;
        $session->execute(new SimpleStatement($cql), []);

        //BILLS TABLE (PK = User email)
        $output->writeln("Creating bills by user email...");
        $table = BillsRepository::getUserEmailTableName();
        $this->cassandra->dropTableIfExists($table);
        $cql =
            <<<EOD
            CREATE TABLE $table(
                    id uuid,
                    company_id uuid,
                    user_email text,
                    amount decimal,
                    PRIMARY KEY (user_email, id)
            )
            EOD;
        $session->execute(new SimpleStatement($cql), []);

        $output->writeln("Database initialized!");
        return Command::SUCCESS;
    }
}