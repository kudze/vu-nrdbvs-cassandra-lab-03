<?php

namespace Kudze\CassandraDemo\Command;

use Cassandra\Uuid;
use Kudze\CassandraDemo\Exceptions\CompanyInsertIdAlreadyInUse;
use Kudze\CassandraDemo\Models\Company;
use Kudze\CassandraDemo\Repository\CompanyRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddCompanyCommand extends Command
{
    protected const ARG_TITLE = "title";

    protected CompanyRepository $companyRepository;

    public function __construct(CompanyRepository $companyRepository)
    {
        parent::__construct();

        $this->companyRepository = $companyRepository;
    }

    protected function configure()
    {
        $this->setName("add-company");
        $this->setDescription("Adds a company");

        $this->addArgument(self::ARG_TITLE, InputArgument::REQUIRED, "Title of a company");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $title = $input->getArgument(self::ARG_TITLE);

        while(true) {
            try {
                $company = new Company(new Uuid(), $title);
                $this->companyRepository->insertCompany($company);

                $output->writeln("Company with a title \"$title\" has been successfully added!");
                break;
            } catch (CompanyInsertIdAlreadyInUse) {
                $output->writeln("Randomly picked UUID for company is already in use, retrying with another one...");
                continue;
            }
        }

        return Command::SUCCESS;
    }

}