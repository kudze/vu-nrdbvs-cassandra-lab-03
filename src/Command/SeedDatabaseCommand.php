<?php

namespace Kudze\CassandraDemo\Command;

use Cassandra\Decimal;
use Cassandra\Uuid;
use Faker\Factory;
use Faker\Generator;
use Kudze\CassandraDemo\Exceptions\BillInsertIdAlreadyInUse;
use Kudze\CassandraDemo\Exceptions\CompanyInsertIdAlreadyInUse;
use Kudze\CassandraDemo\Exceptions\UserInsertEmailAlreadyInUseException;
use Kudze\CassandraDemo\Models\Bill;
use Kudze\CassandraDemo\Models\Company;
use Kudze\CassandraDemo\Models\User;
use Kudze\CassandraDemo\Repository\BillsRepository;
use Kudze\CassandraDemo\Repository\CompanyRepository;
use Kudze\CassandraDemo\Repository\UserRepository;
use Kudze\CassandraDemo\Service\Hasher;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SeedDatabaseCommand extends Command
{
    const AMT_USERS = 5;
    const AMT_COMPANIES = 5;
    const AMT_BILLS = 20;

    protected UserRepository $userRepository;
    protected CompanyRepository $companyRepository;
    protected BillsRepository $billsRepository;
    protected Hasher $hasher;
    protected Generator $generator;

    public function __construct(UserRepository $userRepository, CompanyRepository $companyRepository, BillsRepository $billsRepository, Hasher $hasher)
    {
        parent::__construct();

        $this->userRepository = $userRepository;
        $this->companyRepository = $companyRepository;
        $this->billsRepository = $billsRepository;
        $this->hasher = $hasher;

        $this->generator = Factory::create('lt_LT');
    }

    public function configure()
    {
        $this->setName("seed");
        $this->setDescription("Seeds database with fake data!");
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("Generating users...");
        $userEmails = [];
        for($i = 0; $i < self::AMT_USERS; $i++)
        {
            while(true) {
                try {
                    $user = new User($this->generator->email, $this->hasher->hash($input));
                    $this->userRepository->insertUser($user);

                    $userEmails[] = $user->getEmail();
                    break;
                } catch(UserInsertEmailAlreadyInUseException) {
                    continue;
                }
            }
        }

        $output->writeln("Generating companies...");
        $companyIds = [];
        for($i = 0; $i < self::AMT_COMPANIES; $i++)
        {
            while(true) {
                try {
                    $company = new Company(new Uuid(), $this->generator->company);
                    $this->companyRepository->insertCompany($company);

                    $companyIds[] = $company->getId();
                    break;
                } catch(CompanyInsertIdAlreadyInUse) {
                    continue;
                }
            }
        }

        $output->writeln("Generating bills...");
        for($i = 0; $i < self::AMT_BILLS; $i++)
        {
            while(true) {
                try {
                    $bill = new Bill(
                        new Uuid(),
                        $companyIds[array_rand($companyIds)],
                        $userEmails[array_rand($userEmails)],
                        new Decimal(number_format($this->generator->numberBetween(100, 10000) / 100.0, 2, ".", ""))
                    );
                    $this->billsRepository->insertBill($bill);

                    break;
                } catch(BillInsertIdAlreadyInUse) {
                    continue;
                }
            }
        }

        return Command::SUCCESS;
    }

}