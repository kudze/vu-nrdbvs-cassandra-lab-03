<?php

namespace Kudze\CassandraDemo\Command;

use Cassandra\Uuid;
use Kudze\CassandraDemo\Repository\BillsRepository;
use Kudze\CassandraDemo\Service\TableFormatter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListCompanyBillsCommand extends Command
{
    const ARG_COMPANY_ID = "company_id";

    protected BillsRepository $billsRepository;
    protected TableFormatter $tableFormatter;

    public function __construct(BillsRepository $billsRepository, TableFormatter $tableFormatter)
    {
        parent::__construct();

        $this->billsRepository = $billsRepository;
        $this->tableFormatter = $tableFormatter;
    }

    protected function configure()
    {
        $this->setName("company-bills");
        $this->setDescription("Lists all company bills");

        $this->addArgument(self::ARG_COMPANY_ID, InputArgument::REQUIRED, "Id of a company");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $companyId = $input->getArgument(self::ARG_COMPANY_ID);

        $bills = $this->billsRepository->getBillsByCompanyId(new Uuid($companyId));
        $table = $this->tableFormatter->formatBills($bills);
        $output->writeln($table);

        return Command::SUCCESS;
    }

}