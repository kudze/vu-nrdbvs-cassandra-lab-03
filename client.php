#!/usr/bin/env php
<?php

use Kudze\CassandraDemo\Command\HelloWorldCommand;
use Kudze\CassandraDemo\Command\RegisterUserCommand;
use Kudze\CassandraDemo\Command\SetupCassandraCommand;
use Kudze\CassandraDemo\Command\ListUsersCommand;
use Kudze\CassandraDemo\Command\AddCompanyCommand;
use Kudze\CassandraDemo\Command\ListCompaniesCommand;
use Kudze\CassandraDemo\Command\ListBillsCommand;
use Kudze\CassandraDemo\Command\CreateBillCommand;
use Kudze\CassandraDemo\Command\ListUserBillsCommand;
use Kudze\CassandraDemo\Command\ListCompanyBillsCommand;
use Kudze\CassandraDemo\Command\SeedDatabaseCommand;
use Symfony\Component\Console\Application;

require __DIR__ . '/vendor/autoload.php';

const APP_NAME = "Karolis cassandra demo";
const APP_VER = "alpha 0.0.1";
$app = new Application(
    APP_NAME,
    APP_VER
);

//Register commands.
$container = new DI\Container();
$app->add($container->get(HelloWorldCommand::class));
$app->add($container->get(RegisterUserCommand::class));
$app->add($container->get(SetupCassandraCommand::class));
$app->add($container->get(ListUsersCommand::class));
$app->add($container->get(AddCompanyCommand::class));
$app->add($container->get(ListCompaniesCommand::class));
$app->add($container->get(ListBillsCommand::class));
$app->add($container->get(CreateBillCommand::class));
$app->add($container->get(ListUserBillsCommand::class));
$app->add($container->get(ListCompanyBillsCommand::class));
$app->add($container->get(SeedDatabaseCommand::class));
$app->run();